


AFRAME.registerComponent("handsfree", {
  init: function() {
   // $rig = document.querySelector("#rig");
    const handsfree = new Handsfree({
      hands: {enabled:true,  minDetectionConfidence: 0.7, minTrackingConfidence: 0.9},
      facemesh: false,
      showDebug: true
    });
    //tween = { yaw: 0, pitch: 0, roll: 0, x: 0, y: 0, z: 0 };

    handsfree.plugin.pinchers.enable();
    handsfree.on("finger-pinched-0-1", () => {});
   /* handsfree.use("lookHandsfree", ({ facemesh }) => {
      if (!facemesh) return;

       

      try {
        //console.log(facemesh.multiFaceLandmarks['0']['9'].x)
        const pos = {
          x: (facemesh.multiFaceLandmarks[0][1].x - 0.5) * 50,
          y: (facemesh.multiFaceLandmarks[0][1].y - 0.5) * 25,
          z: -5
        };

        // Use the tweened values instead of the actual current values from webcam
        if (typeof handsfree.data.facemesh !== "undefined") {
          $rig.setAttribute("rotation", `${pos.x} ${pos.y} ${pos.z}`);
          $rig.setAttribute("position", `${pos.x} ${pos.y} ${pos.z}`);
        } else {
          $rig.setAttribute("rotation", `0 0 0`);
          $rig.setAttribute("position", `0 1 -4`);
        }
      } catch (e) {
        //console.log(e)
      }
    });*/

    handsfree.use("snap", ({ hands }) => {
      if (!hands.multiHandLandmarks) return;
      //console.log(hands.pinchState[1][2]) //enhance hand detection
      try{
        if (hands.pinchState[1][2] === "released" || hands.pinchState[0][0]  === "released" ) {
        changeBackground();
        console.log('color change')
      }
      } catch(e) {
        console.log(e)
      }
      
    });

    const body = document.getElementById("body");

     colorIndex = 0;
   /* const colors = ["#FF8B94", "#FFD3B6"];*/

    var changeBackground = () => {
      //console.log(index);
      body.style.background = colors[colorIndex % colors.length];
      colorIndex++;
    };

   // handsfree.start();
  }
});


handsfree.useGesture({
  "name": "start",
  "algorithm": "fingerpose",
  "models": "hands",
  "confidence": 7.5,
  "description": [
    [
      "addCurl",
      "Thumb",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Thumb",
      "HorizontalLeft",
      1
    ],
    [
      "addCurl",
      "Index",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Index",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Middle",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Middle",
      "VerticalUp",
      1
    ],
    [
      "addCurl",
      "Ring",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Ring",
      "VerticalUp",
      1
    ],
    [
      "addCurl",
      "Pinky",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Pinky",
      "VerticalUp",
      1
    ]
  ]
})





//stop

handsfree.useGesture({
  "name": "start",
  "algorithm": "fingerpose",
  "models": "hands",
  "confidence": 7.5,
  "description": [
    [
      "addCurl",
      "Thumb",
      "HalfCurl",
      1
    ],
    [
      "addDirection",
      "Thumb",
      "VerticalUp",
      1
    ],
    [
      "addCurl",
      "Index",
      "FullCurl",
      1
    ],
    [
      "addDirection",
      "Index",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Middle",
      "FullCurl",
      1
    ],
    [
      "addDirection",
      "Middle",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Ring",
      "FullCurl",
      1
    ],
    [
      "addDirection",
      "Ring",
      "VerticalUp",
      1
    ],
    [
      "addCurl",
      "Pinky",
      "FullCurl",
      1
    ],
    [
      "addDirection",
      "Pinky",
      "VerticalUp",
      1
    ],
    [
      "addDirection",
      "Pinky",
      "DiagonalUpRight",
      0.07142857142857142
    ]
  ]
})



/// snap

handsfree.useGesture({
  "name": "start",
  "algorithm": "fingerpose",
  "models": "hands",
  "confidence": 7.5,
  "description": [
    [
      "addCurl",
      "Thumb",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Thumb",
      "HorizontalLeft",
      0.875
    ],
    [
      "addDirection",
      "Thumb",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Index",
      "NoCurl",
      1
    ],
    [
      "addCurl",
      "Index",
      "HalfCurl",
      1
    ],
    [
      "addDirection",
      "Index",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Middle",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Middle",
      "DiagonalUpLeft",
      1
    ],
    [
      "addCurl",
      "Ring",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Ring",
      "VerticalUp",
      1
    ],
    [
      "addCurl",
      "Pinky",
      "NoCurl",
      1
    ],
    [
      "addDirection",
      "Pinky",
      "VerticalUp",
      0.2
    ],
    [
      "addDirection",
      "Pinky",
      "DiagonalUpRight",
      1
    ]
  ]
})




/*



const face = handsfree.data.facemesh;
        const LEFTEYETOP = facemesh.multiFaceLandmarks[0][159].y * 100;
        const LEFTEYEBOTTOM = facemesh.multiFaceLandmarks[0][145].y * 100;
        const RIGHTEYETOP = facemesh.multiFaceLandmarks[0][386].y * 100;
        const RIGHTEYEBOTTOM = facemesh.multiFaceLandmarks[0][374].y * 100;
        const MOUTHTOP = facemesh.multiFaceLandmarks[0][11].y * 100;
        const MOUTHBOTTOM = facemesh.multiFaceLandmarks[0][16].y * 100;

        var getLeftEyeClick = () => {
          let eyeClick = LEFTEYETOP - LEFTEYEBOTTOM;
          if (Math.abs(eyeClick) < 1.3) {
            console.log(eyeClick);
            console.log("closed-eye");
          }
        };

        function detectBlink() {
          const leftEye_l = facemesh.multiFaceLandmarks[0][263];
          const leftEye_r = facemesh.multiFaceLandmarks[0][362];
          const leftEye_t = facemesh.multiFaceLandmarks[0][386];
          const leftEye_b = facemesh.multiFaceLandmarks[0][374];

          const rightEye_l = facemesh.multiFaceLandmarks[0][133];
          const rightEye_r = facemesh.multiFaceLandmarks[0][33];
          const rightEye_t = facemesh.multiFaceLandmarks[0][159];
          const rightEye_b = facemesh.multiFaceLandmarks[0][145];

          const aL = euclidean_dist(
            100 * leftEye_t.x,
            100 * leftEye_t.y,
            100 * leftEye_b.x,
            100 * leftEye_b.y
          );
          const bL = euclidean_dist(
            100 * leftEye_l.x,
            100 * leftEye_l.y,
            100 * leftEye_r.x,
            100 * leftEye_r.y
          );
          const earLeft = aL / (2 * bL);

          const aR = euclidean_dist(
            100 * rightEye_t.x,
            100 * rightEye_t.y,
            100 * rightEye_b.x,
            100 * rightEye_b.y
          );
          const bR = euclidean_dist(
            100 * rightEye_l.x,
            100 * rightEye_l.y,
            100 * rightEye_r.x,
            100 * rightEye_r.y
          );
          const earRight = aR / (2 * bR);

          console.log("-----> " + earLeft + "\t" + earRight);

          if (earLeft < 0.1 || earRight < 0.1) {
            // console.log('blinked')
            return true;
          } else {
            return false;
          }
        }

        function euclidean_dist(x1, y1, x2, y2) {
          return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        }

        detectBlink(); */