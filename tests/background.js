document.querySelector('a-scene').addEventListener('loaded', ()=> {


$rig = document.querySelector("#rig");
const handsfree = new Handsfree({
  hands: true,
  facemesh: true,
  showDebug: true
});
//tween = { yaw: 0, pitch: 0, roll: 0, x: 0, y: 0, z: 0 };

handsfree.plugin.pinchers.enable();
handsfree.on("finger-pinched-0-1", () => {});
handsfree.use("lookHandsfree", ({ facemesh }) => {
  if (!facemesh) return;

  try {
    //console.log(facemesh.multiFaceLandmarks['0']['9'].x)
    const pos = {
      x: (facemesh.multiFaceLandmarks[0][1].x - 0.5) * 50,
      y: (facemesh.multiFaceLandmarks[0][1].y - 0.5) * 25,
      z: -5
    };

    // Use the tweened values instead of the actual current values from webcam
    if (typeof handsfree.data.facemesh !== "undefined") {
      $rig.setAttribute("rotation", `${pos.x} ${pos.y} ${pos.z}`);
      $rig.setAttribute("position", `${pos.x} ${pos.y} ${pos.z}`);
    } else {
      $rig.setAttribute("rotation", `0 0 0`);
      $rig.setAttribute("position", `0 1 -4`);
    }
  } catch (e) {
    console.log(e);
  }
});

handsfree.use("snap", ({ hands }) => {
  if (!hands.multiHandLandmarks) return;
  //console.log(hands.pinchState[1][2])
  if (hands.pinchState[1][2] === "released") {
    changeBackground();
  }
});

const body = document.getElementById("body");

 colorIndex = 0;


var changeBackground = () => {
  //console.log(index);
  body.style.background = colors[colorIndex % colors.length];
  colorIndex++;
};

handsfree.start();
  
  
})