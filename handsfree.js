/** Handsfree module to control hands and face tracking**/

const body = document.getElementById("body");

// sets handsfree config
const handsfree = new Handsfree({
  hands: {
    enabled: true,
    minDetectionConfidence: 0.7,
    minTrackingConfidence: 0.9
  },
  facemesh: true,
  showDebug: true
});

//enables -thumb and index - pinching recognition
handsfree.plugin.pinchers.enable();

// gets facemesh tracking data
handsfree.use("lookHandsfree", ({ facemesh }) => {
  if (!facemesh) return;

  try {
    const camera = document.querySelector("#cam");
    //if (down == 7) {

    const leftEye_l = facemesh.multiFaceLandmarks[0][263];
    const leftEye_r = facemesh.multiFaceLandmarks[0][362];
    const leftEye_t = facemesh.multiFaceLandmarks[0][386];
    const leftEye_b = facemesh.multiFaceLandmarks[0][374];

    const rightEye_l = facemesh.multiFaceLandmarks[0][133];
    const rightEye_r = facemesh.multiFaceLandmarks[0][33];
    const rightEye_t = facemesh.multiFaceLandmarks[0][159];
    const rightEye_b = facemesh.multiFaceLandmarks[0][145];
    //console.log(facemesh.multiFaceLandmarks['0']['9'].x)

    //gets nose position and sets it to x and y
    const nose = facemesh.multiFaceLandmarks[0][1];
    const pos = {
      x: (nose.x - 0.5) * 50,
      y: (nose.y - 0.5) * 25,
      z: -5
    };
    // sets face position to camera position - needs tweening but gsap stopped working dunno why
    camera.setAttribute("position", `${pos.x} ${pos.y} -1`);

    /// TO ADD: head states, as the pinch states of the pinch plugin
    // see: https://handsfree.js.org/ref/plugin/pinchers.html#full-plugin-code

    var headDirStates = ["", "", "", ""];
    let right = 0,
      left = 0,
      up = 0,
      down = 0;
    //
    if (typeof handsfree.data.facemesh !== "undefined") {
      if (pos.x < -4) {
        //console.log("right");
        right++;
      } else if (pos.x > 4) {
        //console.log("left");
        left++;
      }

      //console.log(pos.y)
      if (pos.y < -3) {
        //console.log("up");
        up++;
      } else if (pos.y > 2) {
        //console.log("down");
        down++;
      }

      //console.log(down%7)

      /// Posible implementation of "move to scene X "

      // retrieve head movement direction
      var headMovement = () => {
        let result = "";
        if (down % 9 == 1) {
          console.log("pabajo");
          result == "down";
        } else if (up % 9 == 1) {
          console.log("parriba");
          result == "up";
        } else {
          result == "stay"
        }
        console.log(result)
        return result;
      };

      //  set scene according to the head movement
      var headSetScene = () => {
        console.log(headMovement())
        switch (headMovement()) {
          case "up":
            console.log('parriba');
            //setScene(0);
            break;
          case "down":
            console.log('pabajo');
            //setScene(1);
            break;
        }
      };
      
      //move = headMovement()
      //console.log(move)
      //console.log(index)
      
     // headSetScene()

      // not so good calculation of a blink haha
      const blink_l = Math.abs(leftEye_t.y - leftEye_b.y) * 100;
      //console.log(blink_l);
      let blinkCounter = 0;
      var blinkDetection = () => {
        
      }
      if (blink_l < 1.7) {
        blinkCounter++
      }
      console.log(blinkCounter)
      
      /*if (blinkCounter%4 == 1) {
        console.log(blinkCounter%6)
        console.log("blinked");
      }*/
        
        
    }
  } catch (e) {
    //console.log(e)
  }
});

/// gets the snap to change colour
handsfree.use("snap", ({ hands }) => {
  if (!hands.multiHandLandmarks) return;
  try {
    if (
      hands.pinchState[1][2] === "released" ||
      hands.pinchState[0][0] === "released"
    ) {
      changeBackground();
      console.log("color change");
    }
  } catch (e) {
    console.log(e);
  }
});

colorIndex = 0;

// sets background colour based on the color List
var changeBackground = () => {
  //console.log(index);
  body.style.background = colors[colorIndex % colors.length];
  colorIndex++;
};
