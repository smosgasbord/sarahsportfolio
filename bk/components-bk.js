AFRAME.registerComponent("click-to-shoot", {
  init: function() {
    document.body.addEventListener("mousedown", () => {
      this.el.emit("shoot");
    });
  }
});

AFRAME.registerComponent("init-scene-building", {
  init: function() {
    var el = document.querySelector("#building");
    console.log("initBuilding");
  }
});

/*AFRAME.registerComponent("handsfree-face", {
  init: function() {
    $rig = document.querySelector("#rig");

    window.handsfree = new Handsfree({ weboji: true });

    // Used to hold tween values (without this things will be jerky)
    tween = { yaw: 0, pitch: 0, roll: 0, x: 0, y: 0, z: 0 };

    handsfree.use("lookHandsfree", ({ weboji }) => {
      if (!weboji?.degree?.[0]) return;

      // Calculate rotation
      const rot = weboji.degree;
      rot[0] -= 55;

      // Calculate position
      const pos = {
        x: (weboji.translation[0] - 0.5) * 10,
        y: (weboji.translation[1] - 0.5) * 5,
        z: 5 - weboji.translation[2] * 30
      };

      // Tween this values
      TweenMax.to(tween, 1, {
        yaw: -rot[0] * 1.5,
        pitch: -rot[1] * 1.5,
        roll: rot[2] * 1.5,
        x: pos.x,
        y: pos.y,
        z: pos.z
      });

      // Use the tweened values instead of the actual current values from webcam

      if (handsfree.data.weboji.isDetected) {
        $rig.setAttribute(
          "rotation",
          `${tween.yaw} ${tween.pitch} ${tween.roll}`
        );
        $rig.setAttribute("position", `${tween.x} ${tween.y} ${tween.z}`);
        // console.log(`${tween.yaw} ${tween.pitch} ${tween.roll}`);
        //console.log(`${tween.x} ${tween.y} ${tween.z}`);
      } else {
        $rig.setAttribute("rotation", `0 0 0`);
        $rig.setAttribute("position", `0 1 -4`);
      }
    });

    handsfree.start();

    window.addEventListener("wheel", event => {
      handsfree.stop();
    });
  }
});*/

AFRAME.registerComponent("model-opacity", {
  schema: {
    opacity: { type: "number", default: 0.5 }
  },
  init: function() {
    this.el.addEventListener("model-loaded", this.update.bind(this));
  },
  update: function() {
    var mesh = this.el.getObject3D("mesh");
    var data = this.data;
    if (!mesh) {
      return;
    }
    mesh.traverse(function(node) {
      if (node.isMesh) {
        //console.log(node);
        node.material.opacity = data.opacity;
        node.material.transparent = data.opacity < 1.0;
        node.material.needsUpdate = true;
      }
    });
  }
});

AFRAME.registerComponent("display", {
  init: function() {
    this.el.addEventListener("mouseenter", e => {
      let mesh = this.el.getObject3D("mesh");
      var setOpacity = opacity => {
        mesh.traverse(function(node) {
          if (node.isMesh) {
            node.material.opacity = opacity;
            node.material.needsUpdate = true;
          }
        });
      };

      if (this.el.object3D.scale.x == 0.8) {
        this.el.setAttribute("scale", { x: 0.7, y: 0.7, z: 0.7 });
        this.el.children[0].setAttribute("visible", "false");
        setOpacity(0.3);
      } else {
        this.el.setAttribute("scale", { x: 0.8, y: 0.8, z: 0.8 });
        this.el.children[0].setAttribute("visible", "true");
        setOpacity(1);
      }
    });
  }
});

AFRAME.registerComponent("info-sarah", {
  init: function() {
    setTimeout(() => {
      let x = 0,
        y = 0,
        strength = 600; // higher is stronger
      var mouseMoveHandler = function(e) {
        const data = document.getElementById("data");
        const instructions = document.getElementById("instructions");
        setTimeout(() => {
          if (!data) {
            return;
          } else {
            data.setAttribute("visible", "true");
            instructions.setAttribute("visible", "false");
          }
        }, 700);
        x = e.pageX;
        y = e.pageY;
      };

      var points = Array.from(document.querySelectorAll("a-gltf-model"), el => {
        return {
          gltf: el,
          x: Number(el.object3D.position.x),
          y: Number(el.object3D.position.y),
          ox: Number(el.object3D.position.x),
          oy: Number(el.object3D.position.y)
        };
      });

      // console.log(points);
      var animate = function() {
        let dx, dy, dist, angle;
        points.forEach((el, i) => {
          // start repulsion calculation
          dx = el.x - x;
          dy = el.y - y;
          angle = Math.atan2(dy, dx);
          dist = strength / Math.sqrt(dx * dx + dy * dy);
          el.x += Math.cos(angle) * dist;
          el.y += Math.sin(angle) * dist;
          el.x += (el.ox - el.x) * 0.1;
          el.y += (el.oy - el.y) * 0.1;
          // end repulsion calculation
          el.gltf.object3D.position.x = el.x;
          el.gltf.object3D.position.y = el.y;
          el.gltf.object3D.position.z = el.x;
        });

        window.requestAnimationFrame(animate);
      };

      window.addEventListener("mousemove", mouseMoveHandler);
      animate();
    }, 500);
  }
});

AFRAME.registerComponent("drag-rotate-component", {
  schema: { speed: { default: 2 } },
  init: function() {
    this.ifMouseDown = false;
    this.x_cord = 0;
    this.y_cord = 0;
    document.addEventListener("mousedown", this.OnDocumentMouseDown.bind(this));
    document.addEventListener("mouseup", this.OnDocumentMouseUp.bind(this));
    document.addEventListener("mousemove", this.OnDocumentMouseMove.bind(this));
  },
  OnDocumentMouseDown: function(event) {
    this.ifMouseDown = true;
    this.x_cord = event.clientX;
    this.y_cord = event.clientY;
  },
  OnDocumentMouseUp: function() {
    this.ifMouseDown = false;
  },
  OnDocumentMouseMove: function(event) {
    if (this.ifMouseDown) {
      var temp_x = event.clientX - this.x_cord;
      var temp_y = event.clientY - this.y_cord;
      this.el.object3D.rotateY((temp_x * this.data.speed) / 600);
      /*if(Math.abs(temp_y)<Math.abs(temp_x))
          {
            this.el.object3D.rotateY(temp_x*this.data.speed/600);
          }
          else
          {
            this.el.object3D.rotateX(temp_y*this.data.speed/600);
          }*/
      this.x_cord = event.clientX;
      //this.y_cord = event.clientY;
    }
  }
});

AFRAME.registerComponent("play", {
  init: function() {
    this.onClick = this.onClick.bind(this);
  },
  play: function() {
    window.addEventListener("click", this.onClick);
  },
  pause: function() {
    window.removeEventListener("click", this.onClick);
  },
  onClick: function(evt) {
    var video = document.querySelectorAll("video");
    // console.log(video.length)
    if (!video) {
      return;
    }

    if (video.length > 1) {
      video[0].play();
      video[1].play();
    } else {
      video[0].play();
    }

    /*for (var i; i < video.length;i++) {
      console.log(video.length)
      
    }*/
  }
});

AFRAME.registerComponent("gesture-handler", {
  schema: {
    enabled: { default: true },
    rotationFactor: { default: 5 },
    minScale: { default: 0.3 },
    maxScale: { default: 8 }
  },

  init: function() {
    this.handleScale = this.handleScale.bind(this);
    this.handleRotation = this.handleRotation.bind(this);

    this.isVisible = true;
    this.initialScale = this.el.object3D.scale.clone();
    this.scaleFactor = 1;

    // console.log("workin")
  },

  update: function() {
    if (this.data.enabled) {
      this.el.sceneEl.addEventListener("onefingermove", this.handleRotation);
      this.el.sceneEl.addEventListener("twofingermove", this.handleScale);
    } else {
      this.el.sceneEl.removeEventListener("onefingermove", this.handleRotation);
      this.el.sceneEl.removeEventListener("twofingermove", this.handleScale);
    }
  },

  remove: function() {
    this.el.sceneEl.removeEventListener("onefingermove", this.handleRotation);
    this.el.sceneEl.removeEventListener("twofingermove", this.handleScale);
  },

  handleRotation: function(event) {
    if (this.isVisible) {
      this.el.object3D.rotation.y +=
        event.detail.positionChange.x * this.data.rotationFactor;
      console.log("muevo");
      this.el.object3D.rotation.x +=
        event.detail.positionChange.y * this.data.rotationFactor;
    }
  },

  handleScale: function(event) {
    if (this.isVisible) {
      this.scaleFactor *=
        1 + event.detail.spreadChange / event.detail.startSpread;

      this.scaleFactor = Math.min(
        Math.max(this.scaleFactor, this.data.minScale),
        this.data.maxScale
      );

      this.el.object3D.scale.x = this.scaleFactor * this.initialScale.x;
      this.el.object3D.scale.y = this.scaleFactor * this.initialScale.y;
      this.el.object3D.scale.z = this.scaleFactor * this.initialScale.z;
    }
  }
});

// Component that detects and emits events for touch gestures

AFRAME.registerComponent("gesture-detector", {
  schema: {
    element: { default: "" }
  },

  init: function() {
    this.targetElement =
      this.data.element && document.querySelector(this.data.element);

    if (!this.targetElement) {
      this.targetElement = this.el;
    }

    this.internalState = {
      previousState: null
    };

    this.emitGestureEvent = this.emitGestureEvent.bind(this);

    this.targetElement.addEventListener("touchstart", this.emitGestureEvent);

    this.targetElement.addEventListener("touchend", this.emitGestureEvent);

    this.targetElement.addEventListener("touchmove", this.emitGestureEvent);
  },

  remove: function() {
    this.targetElement.removeEventListener("touchstart", this.emitGestureEvent);

    this.targetElement.removeEventListener("touchend", this.emitGestureEvent);

    this.targetElement.removeEventListener("touchmove", this.emitGestureEvent);
  },

  emitGestureEvent(event) {
    const currentState = this.getTouchState(event);

    const previousState = this.internalState.previousState;

    const gestureContinues =
      previousState &&
      currentState &&
      currentState.touchCount == previousState.touchCount;

    const gestureEnded = previousState && !gestureContinues;

    const gestureStarted = currentState && !gestureContinues;

    if (gestureEnded) {
      const eventName =
        this.getEventPrefix(previousState.touchCount) + "fingerend";

      this.el.emit(eventName, previousState);

      this.internalState.previousState = null;
    }

    if (gestureStarted) {
      currentState.startTime = performance.now();

      currentState.startPosition = currentState.position;

      currentState.startSpread = currentState.spread;

      const eventName =
        this.getEventPrefix(currentState.touchCount) + "fingerstart";

      this.el.emit(eventName, currentState);

      this.internalState.previousState = currentState;
    }

    if (gestureContinues) {
      const eventDetail = {
        positionChange: {
          x: currentState.position.x - previousState.position.x,

          y: currentState.position.y - previousState.position.y
        }
      };

      if (currentState.spread) {
        eventDetail.spreadChange = currentState.spread - previousState.spread;
      }

      // Update state with new data

      Object.assign(previousState, currentState);

      // Add state data to event detail

      Object.assign(eventDetail, previousState);

      const eventName =
        this.getEventPrefix(currentState.touchCount) + "fingermove";

      this.el.emit(eventName, eventDetail);
    }
  },

  getTouchState: function(event) {
    if (event.touches.length === 0) {
      return null;
    }

    // Convert event.touches to an array so we can use reduce

    const touchList = [];

    for (let i = 0; i < event.touches.length; i++) {
      touchList.push(event.touches[i]);
    }

    const touchState = {
      touchCount: touchList.length
    };

    // Calculate center of all current touches

    const centerPositionRawX =
      touchList.reduce((sum, touch) => sum + touch.clientX, 0) /
      touchList.length;

    const centerPositionRawY =
      touchList.reduce((sum, touch) => sum + touch.clientY, 0) /
      touchList.length;

    touchState.positionRaw = { x: centerPositionRawX, y: centerPositionRawY };

    // Scale touch position and spread by average of window dimensions

    const screenScale = 2 / (window.innerWidth + window.innerHeight);

    touchState.position = {
      x: centerPositionRawX * screenScale,
      y: centerPositionRawY * screenScale
    };

    // Calculate average spread of touches from the center point

    if (touchList.length >= 2) {
      const spread =
        touchList.reduce((sum, touch) => {
          return (
            sum +
            Math.sqrt(
              Math.pow(centerPositionRawX - touch.clientX, 2) +
                Math.pow(centerPositionRawY - touch.clientY, 2)
            )
          );
        }, 0) / touchList.length;

      touchState.spread = spread * screenScale;
    }

    return touchState;
  },

  getEventPrefix(touchCount) {
    const numberNames = ["one", "two", "three", "many"];

    return numberNames[Math.min(touchCount, 4) - 1];
  }
});

AFRAME.registerComponent("handsfree", {
  init: function() {
    $rig = document.querySelector("#rig");
    const handsfree = new Handsfree({
      hands: true,
      facemesh: true,
      showDebug: true
    });
    //tween = { yaw: 0, pitch: 0, roll: 0, x: 0, y: 0, z: 0 };

    handsfree.plugin.pinchers.enable();
    handsfree.on("finger-pinched-0-1", () => {});
    handsfree.use("lookHandsfree", ({ facemesh }) => {
      if (!facemesh) return;

       

      try {
        //console.log(facemesh.multiFaceLandmarks['0']['9'].x)
        const pos = {
          x: (facemesh.multiFaceLandmarks[0][1].x - 0.5) * 50,
          y: (facemesh.multiFaceLandmarks[0][1].y - 0.5) * 25,
          z: -5
        };

        // Use the tweened values instead of the actual current values from webcam
        if (typeof handsfree.data.facemesh !== "undefined") {
          $rig.setAttribute("rotation", `${pos.x} ${pos.y} ${pos.z}`);
          $rig.setAttribute("position", `${pos.x} ${pos.y} ${pos.z}`);
        } else {
          $rig.setAttribute("rotation", `0 0 0`);
          $rig.setAttribute("position", `0 1 -4`);
        }
      } catch (e) {
        console.log(e)
      }
    });

    handsfree.use("snap", ({ hands }) => {
      if (!hands.multiHandLandmarks) return;
      //console.log(hands.pinchState[1][2])
      if (hands.pinchState[1][2] === "released") {
        changeBackground();
      }
    });

    const body = document.getElementById("body");

     colorIndex = 0;
   /* const colors = ["#FF8B94", "#FFD3B6"];*/

    var changeBackground = () => {
      //console.log(index);
      body.style.background = colors[colorIndex % colors.length];
      colorIndex++;
    };

    handsfree.start();
  }
});
