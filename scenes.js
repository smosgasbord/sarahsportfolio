const scenes = ["info.html", "balloons.html", "home.html", "projects.html"];
const colors = ["#FF8B94", "#FFD3B6", "#A8E6CF", "#DCEDC1"];
const sceneTemp = document.getElementById("scene-temp");
const maskEl = document.getElementById("mask");
const sky = document.getElementById("sky");
let index = 0;
let newIndex, colorIndex;
var mode;

var sleep = time => {
  return new Promise(resolve => setTimeout(resolve, time));
};

//sets scene
var setScene = sceneIndex => {
  //;

  sceneTemp.setAttribute("template", "src", "scenes/" + scenes[sceneIndex]);
  maskEl.emit("fade");
 /* if (sceneIndex == 1) {
    setTimeout(()=> {
    info = document.getElementById("info-init");
    info.setAttribute("info-sarah", "");},1000)
  }*/

  //index = sceneIndex
};

window.addEventListener("wheel", event => {
  const delta = event.wheelDelta / 120;
  //console.log(delta);
  index = index + delta;
  newIndex = Math.abs(index) % 2;
  console.log(newIndex);
  setScene(newIndex);
  //index = newIndex
  //sleep(800).then(() => {});
});

if (AFRAME.utils.device.isMobile() == false) {
  //handsfree.start()
} else {
  //mobile implementation with sensors
}
